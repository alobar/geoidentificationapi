﻿using GeoIdentificationAPI.Services.Viewmodels;
using GeoIdentificationAPI.Services.ViewModels;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Shouldly;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GeoIdentificationAPI.IntegrationTests
{
    public class BatchGeoIdentificationControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private HttpClient Client { get; }
        private readonly string baseApiUrl = "/api/BatchGeoIdentification";

        public BatchGeoIdentificationControllerTests(WebApplicationFactory<Startup> fixture)
        {
            Client = fixture.CreateClient();
        }

        [Fact]
        public async Task Given_Valid_Addresses_BatchGeoIdentification_Should_Return_BatchId()
        {
            List<string> ipAddresses = new List<string> { "62.1.160.163", "62.1.160.164", "62.1.160.165", "62.1.160.166" };

            HttpResponseMessage response = await Client.PostAsync($"{baseApiUrl}/", new StringContent(JsonConvert.SerializeObject(ipAddresses), Encoding.UTF8, "application/json"));

            response.StatusCode.ShouldBe(HttpStatusCode.OK);

            var batchGeoIdentificationResponseViewModel = JsonConvert.DeserializeObject<BatchGeoIdentificationResponseViewModel>(await response.Content.ReadAsStringAsync());

            batchGeoIdentificationResponseViewModel.ShouldNotBeNull();
            batchGeoIdentificationResponseViewModel.BatchId.ShouldBePositive();
            batchGeoIdentificationResponseViewModel.BatchUrl.ShouldNotBeNull();
        }

        [Fact]
        public async Task Given_Invalid_BatchId_GeoIdentificationBatchStatus_Should_Return_NotFound()
        {

            HttpResponseMessage response = await Client.GetAsync($"{baseApiUrl}/{0}");

            response.StatusCode.ShouldBe(HttpStatusCode.NotFound);           
        }

        [Fact]
        public async Task Given_Valid_BatchId_GeoIdentificationBatchStatus_Should_Return_Statistics()
        {
            List<string> ipAddresses = new List<string> { "62.1.160.163", "62.1.160.164", "62.1.160.165", "62.1.160.166" };
            HttpResponseMessage batchCreationResponse = await Client.PostAsync($"{baseApiUrl}/", new StringContent(JsonConvert.SerializeObject(ipAddresses), Encoding.UTF8, "application/json"));
            BatchGeoIdentificationResponseViewModel batchGeoIdentificationResponseViewModel = JsonConvert.DeserializeObject<BatchGeoIdentificationResponseViewModel>(await batchCreationResponse.Content.ReadAsStringAsync());
            int batchId = batchGeoIdentificationResponseViewModel.BatchId;

            HttpResponseMessage response = await Client.GetAsync($"{baseApiUrl}/{batchId}");

            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            BatchStatisticsViewmodel batchStatisticsViewmodel = JsonConvert.DeserializeObject<BatchStatisticsViewmodel>(await response.Content.ReadAsStringAsync());

            batchStatisticsViewmodel.ShouldNotBeNull();
            batchStatisticsViewmodel.EstimatedRemainingTime.ShouldNotBeNull();
            batchStatisticsViewmodel.Progress.ShouldNotBeNull();
        }
    }
}
