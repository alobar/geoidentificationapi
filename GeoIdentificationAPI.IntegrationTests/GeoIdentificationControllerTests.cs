﻿using GeoIdentificationAPI.Services.Viewmodels;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using Shouldly;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace GeoIdentificationAPI.IntegrationTests
{
    public class GeoIdentificationControllerTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private HttpClient Client { get; }
        private readonly string baseApiUrl = "/api/GeoIdentification";

        public GeoIdentificationControllerTests(WebApplicationFactory<Startup> fixture)
        {
            Client = fixture.CreateClient();
        }

        [Theory]
        [InlineData("2a02:2149:840c:d00:4935:f6ef:9392:ceaa")]
        [InlineData("62.1.160.163")]
        public async Task Given_Valid_Address_Get_Should_Retrieve_Details(string ip)
        {
            HttpResponseMessage response = await Client.GetAsync($"{baseApiUrl}/{ip}");

            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            GeolocationViewModel geolocationViewModel = JsonConvert.DeserializeObject<GeolocationViewModel>(await response.Content.ReadAsStringAsync());
            geolocationViewModel.ShouldNotBeNull();
        }

        [Theory]
        [InlineData("62.1.160.756")]
        [InlineData("ipAddress")]
        public async Task Given_Not_Valid_Address_Get_Should_Return_BadRequest(string ip)
        {
            HttpResponseMessage response = await Client.GetAsync($"{baseApiUrl}/{ip}");

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
    }
}
