﻿using GeoIdentificationAPI.Persistence.Models;
using Microsoft.EntityFrameworkCore;

namespace GeoIdentificationAPI.Persistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
               : base(options)
        {
        }

        public DbSet<GeoIdentificationBatch> GeoIdentificationBatch { get; set; }
        public DbSet<Geolocation> Geolocation { get; set; }
    }
}
