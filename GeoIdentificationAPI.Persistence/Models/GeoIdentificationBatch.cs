﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GeoIdentificationAPI.Persistence.Models
{
    public class GeoIdentificationBatch
    {
        public GeoIdentificationBatch()
        {
            Geolocations = new HashSet<Geolocation>();
        }

        [Key]
        public int Id { get; set; }
        public DateTime DateTimeStarted { get; set; }

        public virtual ICollection<Geolocation> Geolocations { get; set; }
    }
}
