﻿using System.ComponentModel.DataAnnotations;

namespace GeoIdentificationAPI.Persistence.Models
{
    public class Geolocation
    {
        [Key]
        public int Id { get; set; }     
        public string Ip { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string TimeZone { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool Processed { get; set; }

        [Required]
        public int GeoIdentificationBatchId { get; set; }

        public virtual GeoIdentificationBatch GeoIdentificationBatch { get; set; }
    }
}
