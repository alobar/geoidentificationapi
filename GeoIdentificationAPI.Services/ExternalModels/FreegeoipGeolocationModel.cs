﻿namespace GeoIdentificationAPI.Services.ExternalModels
{
    public class FreegeoipGeolocationModel
    {
        public string IP { get; set; }
        public string Country_code { get; set; }
        public string Country_name { get; set; }
        public string Time_zone { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
