﻿using GeoIdentificationAPI.Persistence;
using GeoIdentificationAPI.Persistence.Models;
using GeoIdentificationAPI.Services.Viewmodels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GeoIdentificationAPI.Services
{
    public class GeoIdentificationBatchService : IGeoIdentificationBatchService
    {
        private readonly ApplicationDbContext _context;
        private readonly IGeoIdentificationService _geoIdentificationService;

        public GeoIdentificationBatchService(ApplicationDbContext context, IGeoIdentificationService geoIdentificationService)
        {
            _context = context;
            _geoIdentificationService = geoIdentificationService;
        }

        public async Task<int> CreateGeoIdentificationBatchAsync()
        {
            GeoIdentificationBatch GeoIdentificationBatch = new GeoIdentificationBatch { };
            await _context.GeoIdentificationBatch.AddAsync(GeoIdentificationBatch);
            await _context.SaveChangesAsync();

            return GeoIdentificationBatch.Id;
        }

        public async Task ProcessGeoIdentificationBatchAsync(int id)
        {

            GeoIdentificationBatch geoIdentificationBatch = await _context.GeoIdentificationBatch.Include(i => i.Geolocations).FirstOrDefaultAsync(geolocationBatch => geolocationBatch.Id == id);

            geoIdentificationBatch.DateTimeStarted = DateTime.Now;
            await _context.SaveChangesAsync();

            foreach (Geolocation geoLocation in geoIdentificationBatch.Geolocations)
            {
                GeolocationViewModel geoLocationViewModel = _geoIdentificationService.RetrieveExternalGeolocationDetails(geoLocation.Ip);

                geoLocation.Latitude = geoLocationViewModel.Latitude;
                geoLocation.Longitude = geoLocationViewModel.Longitude;
                geoLocation.CountryCode = geoLocationViewModel.CountryCode;
                geoLocation.CountryName = geoLocationViewModel.CountryName;
                geoLocation.TimeZone = geoLocationViewModel.TimeZone;
                geoLocation.Processed = true;
                await _context.SaveChangesAsync();
            }
        }

        public async Task<BatchStatisticsViewmodel> GetBatchStatisticsAsync(int id)
        {
            GeoIdentificationBatch geoIdentificationBatch = await _context.GeoIdentificationBatch.Include(i => i.Geolocations).FirstOrDefaultAsync(geolocationBatch => geolocationBatch.Id == id);
            if (geoIdentificationBatch == null)
            {
                return null;
            }
            int items = geoIdentificationBatch.Geolocations.Count;
            int completedItems = geoIdentificationBatch.Geolocations.Where(i => i.Processed).Count();
            int remainingItems = items - completedItems;

            if (completedItems == 0)
            {
                return new BatchStatisticsViewmodel { Progress = $"0 / {items}", EstimatedRemainingTime = "Unknown" };
            }

            TimeSpan averageProcessTimePerIp = (DateTime.Now - geoIdentificationBatch.DateTimeStarted) / completedItems;
            return new BatchStatisticsViewmodel { Progress = $"{completedItems} / {items}", EstimatedRemainingTime = $"{averageProcessTimePerIp.TotalMinutes * remainingItems} minutes" };
        }
    }
}
