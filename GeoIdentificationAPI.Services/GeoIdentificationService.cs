﻿using GeoIdentificationAPI.Persistence;
using GeoIdentificationAPI.Persistence.Models;
using GeoIdentificationAPI.Services.ExternalModels;
using GeoIdentificationAPI.Services.Viewmodels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace GeoIdentificationAPI.Services
{
    public class GeoIdentificationService : IGeoIdentificationService
    {
        private readonly ApplicationDbContext _context;
        private readonly string FreegeoipServerUrl = "https://freegeoip.app/json";
        public GeoIdentificationService(ApplicationDbContext context)
        {
            _context = context;
        }  

        public async Task AddGeolocationsDetailsAsync(IEnumerable<string> IpAddresses, int geoIdentificationBatchId )
        {
            foreach (var ipAddress in IpAddresses) 
            {
                Geolocation geolocation = new Geolocation {  Ip = ipAddress, GeoIdentificationBatchId = geoIdentificationBatchId };
                await _context.Geolocation.AddAsync(geolocation);
            }

            await _context.SaveChangesAsync();          
        }

        public GeolocationViewModel RetrieveExternalGeolocationDetails(string ipAddress)
        {
            using WebClient webclient = new WebClient();
            string json = webclient.DownloadString($"{FreegeoipServerUrl}/{ipAddress}");

            FreegeoipGeolocationModel freegeoipGeolocationModel = JsonConvert.DeserializeObject<FreegeoipGeolocationModel>(json);

            return GeolocationViewModel.ToGeolocationViewModel(freegeoipGeolocationModel);
        }

        public bool IsAddressValid(string IpAddress)
        {
            return IPAddress.TryParse(IpAddress, out _);
        }
    }
}
