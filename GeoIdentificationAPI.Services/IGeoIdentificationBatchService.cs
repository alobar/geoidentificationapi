﻿using GeoIdentificationAPI.Services.Viewmodels;
using System.Threading.Tasks;

namespace GeoIdentificationAPI.Services
{
    public interface IGeoIdentificationBatchService
    {
        Task<int> CreateGeoIdentificationBatchAsync();
        Task ProcessGeoIdentificationBatchAsync(int id);
        Task<BatchStatisticsViewmodel> GetBatchStatisticsAsync(int id);
    }
}
