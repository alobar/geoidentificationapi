﻿using GeoIdentificationAPI.Services.Viewmodels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeoIdentificationAPI.Services
{
    public interface IGeoIdentificationService
    {
        GeolocationViewModel RetrieveExternalGeolocationDetails(string IpAddress);
        Task AddGeolocationsDetailsAsync(IEnumerable<string> IpAddresses, int geoIdentificationBatchId);
        bool IsAddressValid(string IpAddress);
    }
}
