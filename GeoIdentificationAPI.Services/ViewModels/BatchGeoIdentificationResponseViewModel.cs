﻿namespace GeoIdentificationAPI.Services.ViewModels
{
    public class BatchGeoIdentificationResponseViewModel
    {
        public string BatchUrl { get; set; }
        public int BatchId { get; set; }
    }
}
