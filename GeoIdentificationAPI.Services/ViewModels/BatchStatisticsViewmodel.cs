﻿namespace GeoIdentificationAPI.Services.Viewmodels
{
    public class BatchStatisticsViewmodel
    {
        public string Progress{ get; set; }
        public string EstimatedRemainingTime { get; set; }
    }
}
