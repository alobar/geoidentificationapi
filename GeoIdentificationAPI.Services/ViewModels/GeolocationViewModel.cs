﻿using GeoIdentificationAPI.Services.ExternalModels;

namespace GeoIdentificationAPI.Services.Viewmodels
{
    public class GeolocationViewModel
    {
        public string IP { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string TimeZone { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public static GeolocationViewModel ToGeolocationViewModel(FreegeoipGeolocationModel freegeoipGeolocationModel)
        {
            return new GeolocationViewModel
            {
                IP = freegeoipGeolocationModel.IP,
                CountryCode = freegeoipGeolocationModel.Country_code,
                CountryName = freegeoipGeolocationModel.Country_name,
                Latitude = freegeoipGeolocationModel.Latitude,
                Longitude = freegeoipGeolocationModel.Longitude,
                TimeZone = freegeoipGeolocationModel.Time_zone
            };
        }
    }
}
