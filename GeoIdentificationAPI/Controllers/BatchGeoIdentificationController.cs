﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GeoIdentificationAPI.Services;
using GeoIdentificationAPI.Services.ViewModels;
using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GeoIdentificationAPI.Helpers;

namespace GeoIdentificationAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatchGeoIdentificationController : ControllerBase
    {
        private readonly IGeoIdentificationService _geoIdentificationService;
        private readonly IGeoIdentificationBatchService _geoIdentificationBatchService;
        private readonly IBackgroundJobClient _jobClient;
        private readonly HttpContext _currentContext;

        public BatchGeoIdentificationController(IGeoIdentificationService geoIdentificationService, IGeoIdentificationBatchService geoIdentificationBatchService, IBackgroundJobClient jobClient, IHttpContextAccessor httpContextAccessor)
        {
            _geoIdentificationBatchService = geoIdentificationBatchService;
            _geoIdentificationService = geoIdentificationService;
            _jobClient = jobClient;
            _currentContext = httpContextAccessor.HttpContext;
        }

        [HttpPost]
        public async Task<IActionResult> BatchGeoIdentification(IEnumerable<string> ipAddresses)
        {
            var batchId = await _geoIdentificationBatchService.CreateGeoIdentificationBatchAsync();
            await _geoIdentificationService.AddGeolocationsDetailsAsync(ipAddresses, batchId);

            _jobClient.Enqueue(() => _geoIdentificationBatchService.ProcessGeoIdentificationBatchAsync(batchId));

            return Ok(new BatchGeoIdentificationResponseViewModel { BatchUrl = $"{_currentContext.ToStringUrl()}/{batchId}", BatchId = batchId });
        }

        [HttpGet("{BatchId}")]
        public async Task<IActionResult> GetBatchGeoIdentificationStatus(int BatchId)
        {
            var statistics = await _geoIdentificationBatchService.GetBatchStatisticsAsync(BatchId);

            if (statistics == null)
            {
                return NotFound();
            }

            return Ok(statistics);
        }
    }
}