﻿using GeoIdentificationAPI.Services;
using GeoIdentificationAPI.Services.Viewmodels;
using Microsoft.AspNetCore.Mvc;

namespace GeoIdentificationAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeoIdentificationController : ControllerBase
    {
        private readonly IGeoIdentificationService _geoIdentificationService;

        public GeoIdentificationController(IGeoIdentificationService geoIdentificationService)
        {
            _geoIdentificationService = geoIdentificationService;
        }

        [HttpGet("{ipAddress}")]
        public IActionResult GetGeoIdentification(string ipAddress)
        {
            if (!_geoIdentificationService.IsAddressValid(ipAddress))
            {
                return BadRequest();
            }

            GeolocationViewModel geolocationDetails = _geoIdentificationService.RetrieveExternalGeolocationDetails(ipAddress);

            return Ok(geolocationDetails);
        }
    }
}