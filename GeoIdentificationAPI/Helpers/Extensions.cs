﻿using Microsoft.AspNetCore.Http;

namespace GeoIdentificationAPI.Helpers
{
    public static class Extensions
    {
        public static string ToStringUrl(this HttpContext currentContext)
        {
            var request = currentContext.Request;
            var host = request.Host.ToUriComponent();
            var pathBase = request.PathBase.ToUriComponent();
            var path = request.Path.ToUriComponent();

            return $"{request.Scheme}://{host}{pathBase}{path}";
        }
    }
}
